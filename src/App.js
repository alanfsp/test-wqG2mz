import logo from './static/images/rd.png';
import './App.scss';
import logoSmall from './static/images/rd-small.png';
import Card from './components/Card'
import PopUp from './components/PopUp'
import TopLinks from './components/TopLinks'
import Description from './components/Description'
import Footer from './components/Footer'
import React ,{useState} from 'react';
import {useRecoilState} from 'recoil';
import {showBlackFridayState} from './state/index';
//import { css } from 'styled-components';
function App() {
  const [showPopUp, setShowPopUp] = useState(false);
  const [showScroll, setShowScroll] = useState(false);

  const [showBlackFriday, setShowBlackFriday] = useRecoilState(showBlackFridayState);
  //const [showBlackFriday, setsShowBlackFriday] = useState(false);
  const changeThemeBlackfridayHandle = ()=>{
    setShowBlackFriday(!showBlackFriday);
  }
  const scrollDownHandle = (show,event=null)=>{
    showScroll? event.target.innerText="Ler mais...": event.target.innerText="Fechar";
    let newHeight="100px";
    setShowScroll(!showScroll);
    setTimeout(()=>{
      let scrolldown = event.target.parentElement.parentElement.querySelector(".scrolldown")
      if(scrolldown){
        scrolldown.style={maxHeight:newHeight}
      }
    },100);

  }
  const cssClass =["container"];
        {
          showBlackFriday && cssClass.push("blackFriday")
        }
  return (
    <>
    <header className={showBlackFriday && ("theme-back-friday")}>
    {showBlackFriday &&(
      <div className="tag">
        <label>Black Friday</label>
      </div>
    )}
      <div className={cssClass.join(' ')}>
        <div className={"rows"}>
          <div className={"logo"}>
            <img className={"logo"} src={logo} alt={"logo"}/>
          </div>
          <div className={"menu"}>
            <TopLinks/>
          </div>
        </div>
      </div>
    </header>
    <main>
    <div className={cssClass.join(' ')}>
      <div className={"rows"}>
        <div className={"col-1"}>
          <Description/>
        </div>
      </div>
      <div className={"rows"}>
        <div className={"col-3"}>
          <Card type={"desktop"} buttonText={"Leia mais..."} functionButton={scrollDownHandle}>
            <p>
              Quando precionado o botão <strong>Leia mais...</strong> o
              restante da informação deverá aparecer em scroll down
            </p>
            {showScroll && (
              <p className={"scrolldown"} style={{maxHeight:0}}>
                Nova informação em scroll down <br/>
                aparecendo com duas linhas
              </p>
            )}
          </Card>
        </div>
        <div className={"col-3"}>
          <Card type={"tablet"} buttonText={"Leia mais..."} functionButton={setShowPopUp}>
            <p>
              Quando precionado o botão <strong>Leia mais...</strong> informação deverá aparecer completa em um popup na tela.
            </p>
          </Card>
        </div>
        <div className={"col-3"}>
          <Card type={"mobile"} buttonText={"alterna tema"} functionButton={changeThemeBlackfridayHandle}>
            <p>
              Quando precionado o botão <strong>alternar tema</strong> modifique o tema do site ara backfriday a seu gosto
              
            </p>
          </Card>
        </div>
      </div>
      {showPopUp && (
      <PopUp functionClose={setShowPopUp}>
        <p>Atualmente, a Droga Raia tem mais de 600 lojas espalhadas pelo Brasil e faz parte do grupo RD, com mais de 1.400 lojas. A Pharmacia Raia foi fundada em 3 de agosto de 1905, na cidade de Araraquara, no interior de São Paulo, a partir de um sonho e com a dedicação de João Baptista Raia, italiano que chegou ao país em 1895 e que se formou em Farmácia e resolveu abrir seu próprio negócio.</p>
      </PopUp>
    )}
    </div>
    
    </main>
    <footer>
      <div className={cssClass.join(' ')}>
      <div className="rows">
        <div className="copyrights">
          <span className="">
            RD 2017 Todos os direitos reservados
          </span>
        </div>
        <div className="icons">
          <Footer/>
        </div>
        <div className="footerLogo">
          <div className="RightAlign">
            <img src={logoSmall} alt="Logo rodape"  />
          </div>

        </div>
      </div>
      </div>
    </footer>
    
    </>
  );
}

export default App;
