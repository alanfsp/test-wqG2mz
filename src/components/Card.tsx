import React from 'react'
import css from './Card.module.scss';
import imageTablet from '../static/images/tablet.png'
import imageDesktop from '../static/images/desktop.png'
import imageMobile from '../static/images/mobile.png'
import {showBlackFridayState} from '../state/index';
import {useRecoilValue} from 'recoil';
 

interface cardPropsInterface{
    type: string,
    children?:React.ReactNode,
    functionButton?:any,
    buttonText:string
}

//export default class Card extends React.Component<cardPropsInterface> {
const  Card = ({type,children,buttonText,functionButton}:cardPropsInterface) => {
    let cssType =null;
    let imgThump = "";
        switch (type) {
            case "tablet":
                imgThump  = imageTablet;
                cssType = css.tablet;
                break;
            case "desktop":
                imgThump  = imageDesktop;
                cssType = css.desktop

                break;
            case "mobile":
                imgThump  = imageMobile;
                cssType = css.mobile;

                break;
        
            default:
                imgThump  =imageDesktop;
                cssType = css.desktop;

                break;
        }
         const showBlackFriday = useRecoilValue(showBlackFridayState);
        
        const cssClass =[css.card];
        {showBlackFriday && cssClass.push(css.blackFriday)}
        
        return (
            <div className={cssClass.join(' ')}>
                {showBlackFriday &&(
                    <div className={css.tag}>
                        <label>Black Friday</label>
                    </div>
                )}
                <div className={`${cssType} ${css.thumb}`}>
                    <img src={imgThump} alt={`image ${type}`}/>
                    <p>Site responsivel <span>{type}</span></p>
                </div>
                
                {children}
                
                <div className={css.actions}>
                    <a className={`${css.readMore} ${cssType}`} onClick={(e)=>{functionButton(true,e)}}>{buttonText}</a>
                </div>
            </div>
        )
}
export default  Card 