import React from 'react'
import css from './TopLinks.module.scss';
import {showBlackFridayState} from '../state/index';
import {useRecoilValue} from 'recoil';
 

//export default class Card extends React.Component<cardPropsInterface> {
const  TopLinks = () => {
    
         const showBlackFriday = useRecoilValue(showBlackFridayState);
        
        const cssClass =[css.topMenu];
        {showBlackFriday &&(
            cssClass.push(css.blackFriday)
        )}
        
        return (
            <div className={cssClass.join(' ')}>
                <ul>
                    <li>
                        <a href="javascript:void(0)">
                            html5
                        </a>
                    </li> 
                   
                    <li>
                        <a href="javascript:void(0)">
                            css3
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            javascript
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            react
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            redux
                        </a>
                    </li>
                </ul>
            </div>
        )
}
export default  TopLinks 