import React from 'react'
import css from './Description.module.scss';
import {showBlackFridayState} from '../state/index';
import {useRecoilValue} from 'recoil';
 

//export default class Card extends React.Component<cardPropsInterface> {
const  Description = () => {
    
         const showBlackFriday = useRecoilValue(showBlackFridayState);
        
        const cssClass =[css.description];
        {showBlackFriday && cssClass.push(css.blackFriday)}
        
        return (
            <div className={cssClass.join(' ')}>
                <h1 >Crie  este site <strong>responsivo</strong> com <strong>REACT</strong> utilizando <strong>styled-components</strong> </h1>
                <p className={css.shortText}>
                    A font utilizada é a Open Sans de 300 a 800.<br/>
                    exemplo: "Open Sans", Helvetica, sans-serif,arial;<br/>
                    Já as cores são:<br/>
                    <span className={`${css.colors} ${css.green}`}>#007f56</span>,
                    <span className={`${css.colors} ${css.gray}`}>#868686</span>,
                    <span className={`${css.colors} ${css.red}`}>#FE9481</span>,
                    <span className={`${css.colors} ${css.yellow}`}>#FCDA92</span> e 
                    <span className={`${css.colors} ${css.purple}`}>#9C8CB9</span>
                </p>
            </div>
        )
}
export default  Description 