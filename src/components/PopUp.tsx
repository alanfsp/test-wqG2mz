import React from 'react'
import css from './PopUp.module.css';


interface popUpPropsInterface{
    children?:React.ReactNode,
    functionClose:any,
}

const PopUp =({children,functionClose}:popUpPropsInterface)=>{
    
        return (
            <div className={css.overlay}>
                <div className={css.popup}>
                    <h2>Atenção</h2>
                    {children}
                    <button onClick={()=>{functionClose(false)}}> Fechar</button>
                </div>
            </div>
        )
}
export default PopUp 