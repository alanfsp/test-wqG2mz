import React from 'react'
import css from './Footer.module.scss';
import {showBlackFridayState} from '../state/index';
import {useRecoilValue} from 'recoil';
import imageRaia from '../static/images/drogaraia.png'
import imageDroga from '../static/images/drogasil.png'
import imageFarm from '../static/images/farmasil.png'
import imageUniv from '../static/images/univers.png'
import image4bio from '../static/images/4bio.png'


//export default class Card extends React.Component<cardPropsInterface> {
const  Footer = () => {
    
         const showBlackFriday = useRecoilValue(showBlackFridayState);
        
        const cssClass =[css.footer];
        {showBlackFriday && cssClass.push(css.blackFriday)}
        
        return (
            <div className={cssClass.join(' ')}>
              <ul>
                  <li><img src={imageRaia} alt="Raia"/></li>
                  <li><img src={imageDroga} alt="Drogasil"/></li>
                  <li><img src={imageFarm} alt="Farmasil" /> </li>
                  <li><img src={imageUniv} alt="Univers"/></li>
                  <li><img src={image4bio} alt="4xbio"/></li>
              </ul>
            </div>
        )
}
export default  Footer 