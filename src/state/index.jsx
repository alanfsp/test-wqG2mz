import {atom} from 'recoil';
  
export const showBlackFridayState = atom({
    key: 'showBlackFridayState', // unique ID (with respect to other atoms/selectors)
    default: false, // default value (aka initial value)
  });
